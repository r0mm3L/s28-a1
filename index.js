console.log('test')

// 3

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
	headers: {
		'Content-Type' : 'application/json'
	}

})
.then((response) => response.json())
.then((json) => console.log(json))


//4 



	async function fetchData() {
		let result = await fetch('https://jsonplaceholder.typicode.com/todos');

		console.log(result);
		console.log(typeof result)
		console.log(result.body)
		// console.log(result.tee)

		let json = await result.json()
		console.log(json)


		let json2 = json.map(json  => json.title)
		console.log(json2)
		
	}

fetchData()


// 5 

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'GET',
	headers: {
		'Content-Type' : 'application/json'
	}

})
.then((response) => response.json())
.then((json) => console.log(json))

// 6

fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'GET',
	headers: {
		'Content-Type' : 'application/json'
	}

})
.then((response) => response.json())

.then((json) => console.log(json.title))




fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'GET',
	headers: {
		'Content-Type' : 'application/json'
	}

})
.then((response) => console.log(response.status))

//7 



	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			completed: true,
			title: 'Implement Route Guard for the HR portal',
			userId: 2
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//8 

	fetch('https://jsonplaceholder.typicode.com/todos/40', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			completed: true,
			title: 'Implement Token Authentication for the HR portal',
			userId: 2
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//9 

	fetch('https://jsonplaceholder.typicode.com/todos/41', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			title: 'Implement a register controller for the HR portal',
			description: 'HR portal project',
			status: 'In progress',
			date_completed: 'not completed yet',
			userId: 41
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//10

	fetch('https://jsonplaceholder.typicode.com/todos/42', {
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			
			completed: true,
			date_completed: 'October 25, 2021'

		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))




